/******************************************************************************

Copyright (c) 2016 by SAS Institute Inc., Cary, NC 27513 USA

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

******************************************************************************/

ML_tables

===============

PDF or PNG tables with brief summaries and best practices for machine learning
and data mining tasks.

Should be printable on 8.5 x 14 in. legal paper.

Contributors include Funda Gunes, Patrick Hall, Jorge Silva, and Brett Wujek

===============

![Alt text](low_res_PNG/MLQuickRefBestPractices.png?raw=true "General Best Practices Table")

===============

![Alt text](low_res_PNG/MLQuickRefAlgos1.PNG?raw=true "Mostly Supervised Algo Table")

===============

![Alt text](low_res_PNG/MLQuickRefAlgos2.PNG?raw=true "Mostly Unupervised Algo Table")
