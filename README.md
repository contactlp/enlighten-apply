enlighten-apply
========================

Example code and materials that illustrate applications of SAS machine learning techniques.

See individual subdirectories for specific examples and instructions. 

Contributors include:
Patrick Hall, Radhikha Myneni, Brett Wujek, and Funda Gunes